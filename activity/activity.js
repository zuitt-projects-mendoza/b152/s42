let inputFirstName = document.querySelector("#txt-first-name");

let fullNameDisplay = document.querySelector("#full-name-display");

let inputLastName = document.querySelector("#txt-last-name");

let form = document.querySelector('#submit-btn');

const showName = () => {

	fullNameDisplay.innerHTML = inputFirstName.value + " " + inputLastName.value;
}

inputFirstName.addEventListener('keyup',showName);
inputLastName.addEventListener('keyup',showName);


form.addEventListener('click',()=>{
	if (inputFirstName.value !== "" && inputLastName.value !== "") {
	    alert("Thank you for registering" + " " + inputFirstName.value + " " + inputLastName.value + "!");
	  } else {
	  	alert("Please input First Name and/or Last Name.")
	  }
})